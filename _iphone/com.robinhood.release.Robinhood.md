---
wsId: Robinhood
title: 'Robinhood: Investing for All'
altTitle: 
authors:
- danny
appId: com.robinhood.release.Robinhood
appCountry: us
idd: 938003185
released: 2014-12-11
updated: 2022-04-11
version: 2022.14.0
stars: 4.2
reviews: 3947689
size: '411340800'
website: https://robinhood.com/
repository: 
issue: 
icon: com.robinhood.release.Robinhood.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive: 
twitter: RobinhoodApp
social:
- https://www.linkedin.com/company/robinhood
- https://www.facebook.com/robinhoodapp

---

{% include copyFromAndroid.html %}
